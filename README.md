# routing-test

Test the routing capabilities provided by Ingress

## Configure
In `docker-compose.yml` replace the default repo path `/d/samu/repos/gitlab/routing-test` with the local path where you just clonned the repo.

## Run
### All containers in compose
`docker-compose up`
On starting up, Ingress should be available at `localhost:4000`.
**Note:** Shall the first attempt to start the composition fail with an error similar to `mkdir /host_mnt/d: file exists`, stop all containers (if any) and re-run `docker-compose up`.

### Independent images
#### Python
`docker run -p 8080:80 -it --rm -v /d/samu/projects/testing-routes/scripts:/usr/src/app -w /usr/src/app python:2 python pythonapp.py`

## Useful commands:
* Start compose: `docker-compose up`
* Stop compose: `docker-compose stop`
* List running containers: `docker ps`
* Kill container: `docker kill <id>`
* Show networks: `docker network ls`
* Inspect network: `docker network inspect <id>`
* Prune stale networks: `docker network prune`
* Prune containers & images `docker container prune ; docker image prune -a`
* Connect to a running service `docker-compose exec <service-id> sh` 
...`<service-id>` is the name of the service as it appears in the `yml` compose config file, and __not__ the name/id of the container listed when doing a `docker-compose`
...The command has to be executed __in the directory where the comfig yml is__ , otherwise the service name will not be recognized
