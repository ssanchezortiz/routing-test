const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  
  const reqMethod = 'METHOD:\n' + req.method + '\n\n';
  const reqUrl = 'URL:\n' + req.url + '\n\n';
  const reqHeaders = 'HEADERS:\n' + JSON.stringify(req.headers) + '\n\n';
  
  let reqBody = [];
  req.on('data', (chunk) => {
    reqBody.push(chunk);
  }).on('end', () => {
    reqBody = 'BODY:\n' + Buffer.concat(reqBody).toString() + '\n\n';
  });  
  
  const responseContent = reqUrl + reqMethod + reqHeaders + reqBody;
  
  res.end(responseContent);
  
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});