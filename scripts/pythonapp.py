
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from optparse import OptionParser
import json

class RequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        
        print("\n----- Request Start ----->\n")
        print(self.path)
        print(self.headers)
        print("<----- Request End -----\n")

        response_msg = "<html>"
        response_msg += "<header><style>table, th, tr, td {width: 100%; border: 1px solid black;}</style></header>"
        response_msg += "<body>"
        response_msg += "<p><b>Requested Path: </b>" + self.path + "</p>"
        response_msg += "<p><b>Headers:</b></p>"
        response_msg += self.__headers_to_html_table(self.headers)
        response_msg += "</body></html>"
        
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(response_msg)
        return

    def __headers_to_html_table(self, headers):

            table = "<table><tr><th>Field</th><th>Value</th></tr>"

            for att in headers:
                table += "<tr><td>" + att + "</td><td>" + headers[att] + "</td></tr>"

            table += "</table>"
            return table


def main():
    port = 80
    print('Listening on localhost:%s' % port)
    server = HTTPServer(('', port), RequestHandler)
    server.serve_forever()

        
if __name__ == "__main__":
    parser = OptionParser()
    parser.usage = ("Creates an http-server that will echo any GET request")
    (options, args) = parser.parse_args()    
    main()